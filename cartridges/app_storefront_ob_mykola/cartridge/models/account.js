'use strict';

var baseAccount = module.superModule;

/**
 * Account class that represents the current customer's profile dashboard
 * @param {Object} currentCustomer - Current customer
 * @param {Object} addressModel - The current customer's preferred address
 * @param {Object} orderModel - The current customer's order history
 * @constructor
 */
function account(currentCustomer, addressModel, orderModel) {
    baseAccount.apply(this, arguments);
    this.profile.myHobby = currentCustomer.raw.profile.custom.myHobby;
    this.profile.myPetName = currentCustomer.raw.profile.custom.myHobby;
    this.profile.myFavoriteMusic = currentCustomer.raw.profile.custom.myFavoriteMusic;
}

module.exports = account;
