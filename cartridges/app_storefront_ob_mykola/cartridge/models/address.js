'use strict';

var baseAddress = module.superModule;

/**
 * Address class that represents an orderAddress
 * @param {dw.order.OrderAddress} addressObject - User's address
 * @constructor
 */
function address(addressObject) {
    baseAddress.apply(this, arguments);

    if ('custom' in addressObject && 'myAddressType' in addressObject.custom) {
        this.address.myAddressType = addressObject.custom.myAddressType;
    } else {
        this.address.myAddressType = 'shipping';
    }
}

module.exports = address;
