'use strict';

var base = require('base/addressBook/addressBook');

base.tabs = function() {
    var inputTypeVal = $('#myAddressType');
    if (inputTypeVal) {
        $('#addressTypeTabs a').on('click', function (event) {
            var tabValue = event.target.hash.replace(/^#/, '');
            inputTypeVal.val(tabValue);
            event.preventDefault();
        });
    }
};

module.exports = base;
